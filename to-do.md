[ ] A lot of sanity checks
[ ] Add an option/subcommand to search text in all diffs (with grep/rg), and select commit
[ ] Optimize filtering commands, prefer awk to multiple cut+sed
[ ] Have a configurable default subcommand
[ ] If 'menu' argument was passed, show a fzf menu to select which type of git object to select (branch, commit, tag, etc.)
