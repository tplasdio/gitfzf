git_remote() {
	local -a preview_bind_args
	local -A preview_bind_cmds

	list_remote_branches() {
		remote="$1"
		for rbranch in $(git branch -r | grep "^ *$remote"); do
			printf -- "\033[1;34m%s\033[m\n" "$rbranch"
			git log -n 5 --date=short \
				--format="%C(green)%C(bold)%cd %C(auto)%h%d %s (%an)" \
				--graph --color=always "$rbranch"
		done
	}

	help_preview() {
		{ printf -- "%s" "$(</dev/stdin)" ;} <<EOF
CTRL-i  Show remote branches' log
CTRL-r  Show remote references (ls-remote)
CTRL-h  Show this help in preview window
EOF
	}

	export -f list_remote_branches help_preview

	preview_bind_cmds=(
		['ctrl-i']='list_remote_branches {1}'
		['ctrl-r']='git ls-remote {1}'
		['ctrl-h']='help_preview'
	)

	preview_bind_args=(
		--bind 'ctrl-i:preview('"${preview_bind_cmds['ctrl-i']}"')'
		--bind 'ctrl-r:preview('"${preview_bind_cmds['ctrl-r']}"')'
		--bind 'ctrl-h:preview('"${preview_bind_cmds['ctrl-h']}"')'
	)

	git_args+=(remote -v)
	fzf_args+=(
		--tac
		--header "CTRL-h to show help in preview window"
		--preview "${preview_bind_cmds['ctrl-i']}"
		"${preview_bind_args[@]}"
	)

	git_cmd \
		| awk '{print $1 "\t" $2}' \
		| uniq \
		| fzf_cmd \
		| cut -d$'\t' -f1
}
