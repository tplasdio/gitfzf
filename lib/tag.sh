git_tag() {
	preview_cmd='git show --color=always {}'
	git_args+=(tag --sort -version:refname)
	fzf_args+=(-m --preview-window right:70% --preview "$preview_cmd")
	git_cmd | fzf_cmd
}
