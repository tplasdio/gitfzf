git_branch() {

	preview_cmd() {
		git log \
			--oneline \
			--graph \
			--date=short \
			--color=always \
			--pretty="format:%C(auto)%cd %h%d %s" \
			$(sed s/^..// <<< "$1" | cut -d" " -f1) \
			--
	}

	export -f preview_cmd

	git_args+=(branch -a --color=always)
	fzf_args+=(-m --ansi --tac --preview-window right:70% --preview "preview_cmd {}")

	git_cmd \
		| grep -v '/HEAD\s' \
		| sort \
		| fzf_cmd \
		| sed 's/^..//' \
		| cut -d' ' -f1 \
		| sed 's#^remotes/##'
}
