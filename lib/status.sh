git_status() {

	preview_status_file() {
		case "$1" in
		('??' | A) cat -- "$2" ;;
		(*) git diff --color=always -- "$2"
		esac
	}

	export -f preview_status_file

	preview_cmd='preview_status_file {1} {-1}'

	git_args+=(-c color.status=always status --short)
	fzf_args+=(-m --ansi --nth 2..,.. --preview "$preview_cmd")

	git_cmd \
		| fzf_cmd \
		| cut -c4- \
		| sed 's/.* -> //'
}
