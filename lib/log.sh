git_log() {
	local -a preview_bind_args
	local -A preview_bind_cmds

	get_commit() {
		grep -o "[a-f0-9]\{7,\}"
	}

	help_preview() {
		{ printf -- "%s" "$(</dev/stdin)" ;} <<EOF
CTRL-i  Show current commit's diff
CTRL-f  Show current commit's changed files
CTRL-s  Toggle sorting
CTRL-h  Show this help in preview window
EOF
	}

	export -f get_commit help_preview

	preview_bind_cmds=(
		['ctrl-i']='git show --color=always "$(get_commit <<< {})"'
		['ctrl-f']='git diff --stat --color=always "$(get_commit <<< {})"'
		['ctrl-h']='help_preview'
	)

	preview_bind_args=(
		--bind 'ctrl-i:preview('"${preview_bind_cmds['ctrl-i']}"')'
		--bind 'ctrl-f:preview('"${preview_bind_cmds['ctrl-f']}"')'
		--bind 'ctrl-h:preview('"${preview_bind_cmds['ctrl-h']}"')'
	)

	git_args+=(
		log
		--date=short
		--format="%C(green)%C(bold)%cd %C(auto)%h%d %s (%an)"
		--graph --color=always
	)
	fzf_args+=(
		-m --ansi --no-sort --reverse
		--bind 'ctrl-s:toggle-sort'
		--header "CTRL-h to show help in preview window"
		--preview "${preview_bind_cmds['ctrl-i']}"
		"${preview_bind_args[@]}"
	)

	git_cmd "$@" \
		| fzf_cmd \
		| get_commit
}
