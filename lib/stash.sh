git_stash() {
	preview_cmd='git show --color=always {1}'
	git_args+=(stash list)
	fzf_args+=(--reverse -d: --preview "$preview_cmd")

	git_cmd \
		| fzf_cmd \
		| cut -d: -f1
}
