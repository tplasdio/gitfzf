# Changelog

All notable changes to this project will be documented in this file.

## [0.2.0] - 2023-08-14

### ⭐ Features

- *(checkout+switch)* New switch and checkout commands as simple wrappers
- *(help)* Add help subcommand and descriptions of subcommands
- *(log)* Add help preview window
- *(log)* Add fzf preview bind for seeing files changed in a commit
- *(log)* Allow passing arguments to and add help arguments
- *(remote)* Add preview bind to see remote references
- *(remote)* Preview now shows short log of each remote's branch
- *(shell)* [**breaking**] Change shell to bash for better refactoring
- *(subcommand)* Rename script to be used as subcommand
-  [**breaking**] Invalid subcommands throw error message and no longer default to log
-  [**breaking**] Add getoptions parser and better help message
-  Add SCCS `what` description

### 🪛 Bug Fixes

- *(branch)* Preview now does not error out if a file and branch are homonymous
- *(log)* Missing log argument
-  Regression on branch preview

### 🔧 Refactor

- *(status)* Redo previewing as function
-  Fzf function now takes environment variable to prevent ARG_MAX issues
-  Better error codes
-  Move subcommand functions to files in lib directory
-  Move script to src directory
-  Remote preview command as exported function, not embedded string
-  Rename fzf_down to fzf_cmd
-  Put argument handling in arrays for each specific subcommand

### 📄 Documentation

- *(changelog)* Add changelog
- *(readme)* Update installation instructions for other Linux distros
- *(readme)* Update installation instructions for Arch
- *(readme)* Update installation instructions
- *(readme)* Add dependecies, update license and formatting
- *(readme)* Add similar section
- *(readme+help)* Improve usage section with help message

### 🎨 Styling

-  Move and update to-do comments to a file
-  Prefer compact style of case statements

### ⚙  Miscellaneous Tasks

- *(license)* Add notice to main file
- *(license)* Change license to AGPL-3.0-or-later

### 📦 Build

- *(packaging)* Add LURE script for packaging to other Linux distros
- *(packaging)* Add missing dependency for Arch Linux
- *(packaging)* Add PKGBUILD-git for Arch Linux
-  Add target to quickly rebuild and install
-  License installation
-  Move to autotools for build system, library path now depends on configure --prefix
-  Makefile now installs and uninstall the libraries
-  Add Makefile, with install and uninstall targets for root or not

## [0.1.0] - 2023-08-14

### ⭐ Features
- Fuzzy selection of commits, branches, tags, stashes, remotes, files in status
- POSIX shell

### ⚙  Miscellaneous Tasks

- *(org)* Initial commit, bump version
- *(license)* GPL-3.0-or-later
