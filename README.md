<p align="center">
  <h1 align="center">git-fzf</h1>
  <p align="center">
    git + fzf selection
  </p>
</p>

<p align="center">
  <img
    width="150"
    src="https://codeberg.org/tplasdio/gitfzf/raw/branch/main/media/gitfzf.svg"
    alt="gitfzf Logo"
  />
</p>

## 📰 Description

A simple script to combine git commands with fzf
for interactive fuzzy selection and nice looking
preview windows.

## 📟 Usage

```
git fzf [OPTIONS] [SUBCMD] [--] [ARGS]

Options:
  -V, --version     Show version
  -h, --help        Show help

Subcommands:
  log               Fuzzy select a commit, subcommand by default
  branch            Fuzzy select a branch
  diff, status      Fuzzy select a file in git-status
  tag               Fuzzy select a tag
  remote            Fuzzy select a remote
  stash             Fuzzy select a stash
  checkout branch   Fuzzy checkout branch
  checkout commit   Fuzzy checkout commit
  help              Show help
```

## 🚀 Installation

<details>
<summary>📦 From source</summary>

- *Dependencies*:
  - `bash`
  - `git`
  - [`fzf`](https://github.com/junegunn/fzf)
  - [`getoptions`](https://github.com/ko1nksm/getoptions)
  - Unix commands: `cut`, `sed`, `awk`, `uniq`, `sort`

- *Build Dependencies*:
  - GNU autotools: `autoreconf`

```sh
git clone --filter=blob:none https://codeberg.org/tplasdio/gitfzf.git
cd gitfzf
autoreconf -i
./configure                       # For system-installation
#./configure --prefix ~/.local    # For user-installation
sudo make install
```

</details>

<details>
<summary>Arch Linux or other pacman distros</summary>

```sh
curl -O https://codeberg.org/tplasdio/gitfzf/raw/branch/main/packaging/PKGBUILD-git
PACMAN=yay makepkg -sip PKGBUILD-git  # or your AUR helper
```

For uninstallation, run `sudo pacman -Rsn gitfzf-git`.

</details>

<details>
<summary>Other Linux</summary>

- Install [`lure`](https://gitea.elara.ws/Elara6331/lure/releases/latest)
- Install [`getoptions`](https://github.com/ko1nksm/getoptions)

```sh
curl -O https://codeberg.org/tplasdio/gitfzf/raw/branch/main/packaging/lure-git.sh
lure build -s lure-git.sh
```

- Debian, Ubuntu or other .deb distros: `apt install ./gitfzf-git*.deb`
- Fedora, OpenSUSE or other .rpm distros: `rpm -i gitfzf-git*.rpm`
- Alpine Linux: `apk add --allow-untrusted gitfzf-git*.apk`

</details>

## 💭 Similar

- [git-fuzzy](https://github.com/bigH/git-fuzzy)
- [forgit](https://github.com/wfxr/forgit/actions)

You may also be looking for a git TUI client like:
- [tig](https://jonas.github.io/tig/)
- [lazygit](https://github.com/jesseduffield/lazygit)
- [gitui](https://github.com/extrawurst/gitui)
- [giter](https://gitlab.com/refaelsh/giter)

## 📝 License

AGPL-3.0-or-later
