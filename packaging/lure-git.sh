name=gitfzf-git
_name=gitfzf
desc="git + fzf selection"
version=.r32.78b4a81
release=1
maintainer="tplasdio <tplasdio cat codeberg dog com>"
licenses=('AGPL-3.0-or-later')
homepage="https://codeberg.org/tplasdio/gitfzf"
deps=(
 coreutils
 git
 fzf
 bash
)
build_deps=(
 automake
 autoconf
)
architectures=('all')
sources=("git+$homepage")
checksums=('SKIP')

version() {
	cd "$srcdir/$_name"
	git-version
}

build() {
	cd "$srcdir/$_name"
	autoreconf -i
	./configure --prefix /usr
	make
}

package() {
	cd "$srcdir/$_name"
	make DESTDIR="$pkgdir" install
}
